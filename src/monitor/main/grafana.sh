#!/bin/bash

curl -XPOST -i http://localhost:3000/api/dashboards/db --data-binary @/root/demo/consul_dash_01.json -H "Content-Type: application/json" -u admin:admin
curl -XPOST -i http://localhost:3000/api/dashboards/db --data-binary @/root/demo/yc_alb_dash_01.json -H "Content-Type: application/json" -u admin:admin