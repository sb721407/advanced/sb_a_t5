#!/bin/sh

go mod download
GO111MODULE=on CGO_ENABLED=0 GOOS=linux go fmt -x ./...