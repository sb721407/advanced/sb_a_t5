version=0.1.12
git tag | xargs git tag -d
git add .
git commit --allow-empty -m "ver $version"
git tag $version
git push -uf origin main --tags
